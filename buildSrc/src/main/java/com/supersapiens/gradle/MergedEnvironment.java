package com.supersapiens.gradle;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import org.gradle.api.Project;

import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class represents the merged build environment, with priority
 * ordering applied to configuration sources.
 */
public final class MergedEnvironment {
    // This file contains default values for every setting.
    private static final String DEFAULTS_FILE = "build_environment_defaults.json";

    // This file contains user overrides for the above defaults.
    private static final String USER_FILE = "build_environment.json";

    private static JsonParser jsonParser;

    private final JsonObject defaults;
    @Nullable
    private final JsonObject userOverrides;

    static MergedEnvironment load(Project project) throws IOException {
        JsonObject defaults = readJsonObject(project.file(DEFAULTS_FILE));

        if (defaults == null) {
            throw new IllegalStateException("Missing " + DEFAULTS_FILE);
        }

        return new MergedEnvironment(
            defaults,
            readJsonObject(project.file(USER_FILE))
        );
    }

    public MergedEnvironment(
        JsonObject defaults,
        @Nullable JsonObject userOverrides
    ) {
        this.defaults = defaults;
        this.userOverrides = userOverrides;
    }

    /**
     * Get all valid setting names.
     *
     * @return All setting names.
     */
    Set<String> getSettings() {
        return defaults.keySet();
    }

    /**
     * Generates a valid `awsconfiguration.json` using values configured
     * in the build environment.
     *
     * @return A map-based representation of the generated configuration.
     */
    Map<String, Object> generateAwsConfiguration() {
        HashMap<String, Object> config = new HashMap<>();
        HashMap<String, Object> userPool = new HashMap<>();
        HashMap<String, Object> defaultPool = new HashMap<>();
        defaultPool.put("PoolId", getValue("COGNITO_USER_POOL_ID"));
        defaultPool.put("AppClientId", getValue("COGNITO_APP_CLIENT_ID"));
        defaultPool.put("AppClientSecret", getValue("COGNITO_APP_CLIENT_SECRET"));
        defaultPool.put("Region", getValue("COGNITO_USER_POOL_REGION"));
        userPool.put("Default", defaultPool);
        config.put("CognitoUserPool", userPool);
        return config;
    }

    /**
     * Gets a setting value from the highest priority source that
     * supplies it.
     *
     * @param setting The name of the setting.
     * @return The setting value.
     */
    @Nullable
    String getValue(String setting) {
        // Environment variables have the highest priority...
        String env = System.getenv(setting);
        if (isNotNullOrBlank(env)) {
            return env;
        }

        // ... then overrides from the `build_environment.json`.
        if (userOverrides != null) {
            JsonPrimitive override = userOverrides.getAsJsonPrimitive(setting);
            if (override != null) {
                String overrideValue = override.getAsString();
                if (isNotNullOrBlank(overrideValue)) {
                    return overrideValue;
                }
            }
        }

        // Fall back on the default from `build_environment_defaults.json`.
        JsonPrimitive defaultPrimitive = defaults.getAsJsonPrimitive(setting);
        if (defaultPrimitive == null) {
            throw new IllegalStateException("Missing default value for " + setting);
        }

        return defaultPrimitive.getAsString();
    }

    private boolean isNotNullOrBlank(@Nullable String s) {
        return s != null && s.trim().length() > 0;
    }

    @Nullable
    private static JsonObject readJsonObject(File file) throws IOException {
        try (Reader reader = new BufferedReader(new FileReader(file))) {
            return getJsonParser().parse(reader).getAsJsonObject();
        } catch (FileNotFoundException e) {
            // A missing file is not necessarily an error, but any other
            // exception indicates a problem and should bubble up.
            return null;
        }
    }

    private static JsonParser getJsonParser() {
        if (jsonParser == null) jsonParser = new JsonParser();
        return jsonParser;
    }
}
