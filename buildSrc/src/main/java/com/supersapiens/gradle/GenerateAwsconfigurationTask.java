package com.supersapiens.gradle;

import com.google.gson.Gson;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.TaskAction;

import javax.inject.Inject;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public abstract class GenerateAwsconfigurationTask extends DefaultTask {
    // This is the directory under the `build` directory where
    // generated files are written.
    private static final String GENERATED_PATH = "generated/build_environment";

    @Inject public GenerateAwsconfigurationTask() { }

    @OutputDirectory
    File getResourceDir() {
        return new File(new File(getProject().getBuildDir(), GENERATED_PATH), "res");
    }

    @TaskAction
    void generate() throws IOException {
        MergedEnvironment env = MergedEnvironment.load(getProject());

        File rawDir = new File(getResourceDir(), "raw");
        rawDir.mkdirs();

        File configFile = new File(rawDir, "awsconfiguration.json");
        try (Writer w = new BufferedWriter(new FileWriter(configFile))) {
            new Gson().toJson(env.generateAwsConfiguration(), w);
        }
    }
}
