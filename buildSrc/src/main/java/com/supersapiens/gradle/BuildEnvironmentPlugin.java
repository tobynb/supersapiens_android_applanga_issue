package com.supersapiens.gradle;

import com.android.build.gradle.AppExtension;
import com.android.build.gradle.tasks.GenerateBuildConfig;
import groovy.json.StringEscapeUtils;
import org.gradle.api.GradleException;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;

import java.io.IOException;

/**
 * This plugin contains logic for configuring the app build using values
 * extracted from the build environment in this order:
 *
 * 1. Compile-time environment variables.
 * 2. Settings in `build_environment.json`.
 * 3. Defaults in `build_environment_defaults.json`.
 *
 * It uses these configured values to set `BuildConfig` fields and
 * generate the `awsconfiguration.json` file needed to access AWS resources.
 */
public class BuildEnvironmentPlugin implements Plugin<Project> {
    private AppExtension android;
    private Project project;

    @Override
    public void apply(Project project) throws GradleException {
        project.getPlugins().withId("com.android.application", plugin -> {
            project.getExtensions().configure(AppExtension.class, android -> {
                this.android = android;
                this.project = project;

                try {
                    addBuildConfigFields();
                    createAwsconfigurationTask();
                } catch (IOException e) {
                    throw new GradleException(e.getMessage());
                }
            });
        });
    }

    private void addBuildConfigFields() throws IOException {
        MergedEnvironment env = MergedEnvironment.load(project);

        for (String setting : env.getSettings()) {
            String value = env.getValue(setting);

            // BuildConfig values are written into generated Java source
            // files, so string values must be surrounded in unescaped
            // double quotes, but any quotes or other special characters
            // contained within the value itself must be escaped.
            String escaped = "\"" + StringEscapeUtils.escapeJava(value) + "\"";

            android.getDefaultConfig().buildConfigField("String", setting, escaped);
        }
    }

    private void createAwsconfigurationTask() {
        Task task = project.getTasks().create(
            "generateAwsconfiguration",
            GenerateAwsconfigurationTask.class
        );

        // Build `awsconfiguration.json` at the same time as `BuildConfig`.
        project.getTasks().withType(GenerateBuildConfig.class, t ->
            t.dependsOn(task)
        );

        android.getApplicationVariants().all(variant ->
            variant.registerGeneratedResFolders(project.files(task))
        );
    }
}
