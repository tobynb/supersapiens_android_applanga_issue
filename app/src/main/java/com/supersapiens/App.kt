package com.supersapiens

import com.applanga.android.ApplangaApplication
import com.supersapiens.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject

class App : ApplangaApplication(), HasAndroidInjector {
    @Inject lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate() {
        Timber.plant(Timber.DebugTree())
        super.onCreate()
        DaggerAppComponent.builder().build().also { it.inject(this) }
    }
}
