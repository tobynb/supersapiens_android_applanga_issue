package com.supersapiens.di.module

import com.supersapiens.screens.activity.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AppModule {
    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity
}
